# uniapp-web
uniapp - 实现一些常用效果、封装通用组件和工具类 <br>

## 项目结构
```
├── common  				//公共文件
│   ├── style	  			//公共样式
│   └── utils	 		    //工具
├── components  			//自定义组件
├── configs					
│   ├── apiConfig.js   		//接口配置
│   └── projectConfig.js 	//项目配置
├── hybrid					//各平台静态页面（待添加）
│   └── html 
├── pages 					//前端页面
│   ├── index
│   ├── login
│   ├── module1
│   ├── module2
│   ├── module3
│   └── module4
├── platforms				//各平台文件（待添加）
│   └── app-plus
├── static 					//静态文件
│   ├── fonts
│   ├── images
├── uni_modules				//uni-app 插件
├── unpackage				//打包文件
├── wxcomponents			//微信运行文件
│   └── vant
├── App.vue					//主组件,页面入口
├── main.js					//uni-app的入口文件，主要作用是初始化vue实例、定义全局组件、使用需要的插件如vuex。
├── manifest.json			//应用的配置文件，用于指定应用的名称、图标、权限等
├── package.json			//支持依赖
├── pages.json				//文件的路径、窗口样式、原生的导航栏、底部的原生tabbar 等
├── uni.scss				//方便整体控制应用的风格。比如按钮颜色、边框风格
```

## 实现的一些效果
- 三方通用组件：`uni`组件（在 uni_modules 文件夹）和`uView`组件([https://www.uviewui.com/](uView官网))，刷新使用的`mescroll-uni`，`vant` 只在小程序使用
- ListView 相关
- GridView 相关
- uni 网络请求封装和 api 配置
- AES、RSA加解密，SHA256、MD5加密，base64编码解码
- 数据缓存
- 全局常量、变量
- 公共样式
- 基类组件: base-navbar、base-refresh-view
- 网络监听，设备类型和刘海屏判断
- 时间格式转换、正则校验、
- 封装一些组件(在 `components` 文件夹)和工具类(在 `utils` 文件夹)

## uniapp注意的点
- 尺寸单位：rpx(根据页面屏幕宽度缩放), 设计师按750px屏宽出图，程序员直接按rpx写代码即可。[详见](https://uniapp.dcloud.io/adapt?id=_3-%e5%86%85%e5%ae%b9%e7%bc%a9%e6%94%be%e6%8b%89%e4%bc%b8%e7%9a%84%e5%a4%84%e7%90%86)
- [插件市场](https://ext.dcloud.net.cn/) 
- [开发环境和生产环境](https://uniapp.dcloud.net.cn/frame?id=%e5%bc%80%e5%8f%91%e7%8e%af%e5%a2%83%e5%92%8c%e7%94%9f%e4%ba%a7%e7%8e%af%e5%a2%83)
- [判断平台](https://uniapp.dcloud.net.cn/frame?id=%e5%88%a4%e6%96%ad%e5%b9%b3%e5%8f%b0)
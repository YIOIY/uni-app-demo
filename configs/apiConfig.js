/* 接口管理 */
var HTTP = require('../common/utils/httpUtils.js');

/**
 * 根据uniapp环境配置不通接口地址
 */
var API_BASE_URL = 'http://10.53.157.42:16011';
if(process.env.NODE_ENV === 'production'){
	API_BASE_URL = 'http://10.53.157.42:16011'	
}

/* 接口地址： */
var URL = {
	getNewPageArrDic: API_BASE_URL + '/mock/pages', // 获取分页数组（新） - get
	getPageArrDic: API_BASE_URL + '/mock/pages', // 获取分页数组 - post
	getSimpleArrDic: API_BASE_URL + '/getSimpleArrDic', // 获取数组
	/*----------------------------------- 首页 -----------------------------------*/
	addHomeData: API_BASE_URL + "/home/add",
	getData: API_BASE_URL + "/home/list",
	/*----------------------------------- 我的 -----------------------------------*/
	getTagInfo: API_BASE_URL + "/hsjry-user-deploy/user/tag_info/query",
}

/* 通过module.exports方式提供给外部调用 */
module.exports = {
	URL,
	getTagInfo: (params) => HTTP.get(URL.getTagInfo, params,'正在加载中...'), //获取分页数据（新）
	getNewPageArrDic: (params) => HTTP.get(URL.getNewPageArrDic, params,'正在加载中...'), //获取分页数据（新）
	getPageArrDic: (params) => HTTP.post(URL.getPageArrDic, params),	//获取分页数据
	getPageArrDic2: (params) => HTTP.post(URL.getPageArrDic, params, '正在加载中...'),	//获取分页数据2
	getSimpleArrDic: (params) => HTTP.post(URL.getSimpleArrDic, params),
	/*----------------------------------- 首页 -----------------------------------*/
	addHomeData: (params) => HTTP.post(URL.addHomeData, params, '正在提交...'),
	getData: (pathParams) => HTTP.get(URL.getData + pathParams),
	/*----------------------------------- 我的 -----------------------------------*/
}


/* 
使用方法 ：

1.在要使用的js文件导入
var API = require('../../configs/APICongfig.js'); 


2. 调用
API.getPageArrDic(params).then(res => {

}).catch(error => {

});

 */

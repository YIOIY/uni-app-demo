// appName
export const AppName = 'uniapp-web';
// 应用版本号
export const LVersion = '1.0.0';

/********************************* 数据存储 相关的 key ********************************/

// 保存本地的用户信息
export const LUserInfo = 'LUserInfo';
// 上次版本号
export const LLastVersion = 'LLastVersion';
//token
export const LToken = 'LToken';

/* 项目配置

  //单页面导入调用

  import ProjectConfig from '@/configs/projectConfig.js'
  const ProjectConfig = require('@/configs/projectConfig.js')
  const ProjectConfig = require('../../configs/projectConfig.js')
  
  console.log(ProjectConfig.LVersion); 
  
  
  
  //main.js挂载调用
  
  // // 导入js文件
  // import ProjectConfig from './common/configs/projectConfig'
  // // 挂载
  // Vue.prototype.$ProjectConfig = ProjectConfig
  
  // module.exports = {
  // 	kTest: 'kTest123',
  // }
   console.log(this.$ProjectConfig.kTest); 
   
 */

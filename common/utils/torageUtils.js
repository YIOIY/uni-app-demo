let isDevelop = process.env.NODE_ENV === 'development';

module.exports = {
	GetStorageSync,
	SetStorageSync,
	RemoveStorageSync,
	Clear,
	GetStorageInfoSync,
};

function GetStorageSync(key) {
	try {
		return uni.getStorageSync(key)
	} catch (e) {
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('key:' + key + '取数据失败：' + e);
		}
		return null
	}
}

function SetStorageSync(key, data) {
	try {
		uni.setStorageSync(key, data);
	} catch (e) {
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('key:' + key + '存数据失败：' + e);
		}
	}
}

function RemoveStorageSync(key) {
	try {
		uni.removeStorageSync(key);
	} catch (e) {
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('key:' + key + '删数据失败：' + e);
		}
	}
}

function GetStorageInfoSync() {
	try {
		const res = uni.getStorageInfoSync();
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('keys:' + res.keys);
			console.log('currentSize:' + res.currentSize);
			console.log('limitSize:' + res.limitSize);
		}
		return res
	} catch (e) {
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('获取本地Info失败：' + e);
		}
	}
}

function Clear() {
	try {
		uni.clearStorageSync();
	} catch (e) {
		if (isDevelop) {
			console.log('===== torageUtils =====');
			console.log('清空本地数据失败：' + e);
		}
	}
}



/*
  使用方法：

  import TorageUtils from '@/common/utils/torageUtils.js'
  const TorageUtils = require('@/common/utils/torageUtils.js')

  TorageUtils.SetStorageSync('key1', 'value1111')
  TorageUtils.GetStorageSync('key1')
  console.log(TorageUtils.GetStorageInfoSync());

  */

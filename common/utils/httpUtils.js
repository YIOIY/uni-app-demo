/* 网络请求工具类 */
const ProjectConfig = require('../../configs/projectConfig.js')
const TorageUtils = require('./torageUtils.js')

var isProduct = process.env.NODE_ENV === 'production';
var isDevelop = process.env.NODE_ENV === 'development';

/* 请求头 */
var _header = {
	'content-type': 'application/json',
	'version': ProjectConfig.LVersion,
}

function getHeader() {
	let LToken = TorageUtils.GetStorageSync(ProjectConfig.LToken)
	if (isDevelop) {
		console.log("===== Token =====", LToken)
	}
	if (LToken) {
		_header.token = LToken
	}
}

//处理发送的数据，对数据加密
function handleSendData(params) {
	if (isDevelop) {
		console.log("===== httpUtils 请求参数 =====", params);
	}
	return params;
}

//处理返回数据，对数据解密
function handleReturnData(res) {
	if (isDevelop) {
		console.log("===== httpUtils 返回数据 =====", res.data);
	}
	return res;
}
//处理返回错误数据，对数据解密
function handleReturnErrorData(res) {
	if (isDevelop) {
		console.log("===== httpUtils 返回error =====", res);
	}
	return res;
}

/**
 * function: 显示/隐藏加载框
 * @isShow 显示/隐藏
 * @loadingText 加载框文字
 */
function showLoading(isShow, loadingText) {
	if (isShow == false) {
		uni.hideLoading()
		return
	}
	if (loadingText == undefined) {} else {
		if (loadingText != "" && isShow == true) {
			uni.showLoading({
				title: loadingText,
			})
		}
	}
}

function showToast(title, icon, duration) {
	uni.showToast({
		title: title,
		icon: icon,
		duration: duration
	})
}

/* 进行请求 */
const request = (url, method, params, loadingText) => {
	getHeader()
	var timeoutID = setTimeout(() => {
		showLoading(true, loadingText)
	}, 300);
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: method,
			data: handleSendData(params),
			header: _header,
			success(res) {
				resolve(handleReturnData(res.data))
			},
			fail(error) {
				uni.showModal({
					title: "请求失败",
					content: error.errMsg,
					showCancel: false,
					confirmText: "重试",
					success: async function() {
						let result = await request(url, method, params,loadingText);
						if (result) {
							resolve(result)
						} else {
							reject(result)
						}
					}
				})
				return;
				// reject(handleReturnErrorData(error.errMsg))
			},
			complete() {
				showLoading(false)
				clearTimeout(timeoutID);
			}
		})
	});
}

/* get请求 */
function get(url, params, loadingText) {
	return request(url, "GET", params, loadingText);
}

/* post请求 */
function post(url, params, loadingText) {
	return request(url, "POST", params, loadingText);
}

/* 文件上传 */
const uploadFile = (url, filePath, params, loadingText) => {
	// console.log("-----文件上传------");
	showLoading(true, loadingText)
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: url,
			name: 'file',
			filePath: filePath,
			formData: handleSendData(params),
			header: _header,
			success(res) {
				resolve(handleReturnData(JSON.parse(res.data)))
			},
			fail(error) {
				reject(error)
			},
			complete: info => {
				showLoading(false)
			}
		})
	})
}

/* 图片加载 */
const loadImage = (url, params) => {
	// console.log("-----图片下载------");
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: 'GET',
			data: params,
			header: _header,
			responseType: 'arraybuffer',
			success(res) {
				if (res.statusCode == 200 && res.data.byteLength) {
					let base64 = wx.arrayBufferToBase64(res.data);
					let img = 'data:image/jpeg;base64,' + base64
					resolve(img)
				} else {
					resolve(null)
				}
			},
			fail(error) {
				reject(error)
			},
			complete: info => {}
		})
	})
}

/* 通过module.exports方式提供给外部调用 */
module.exports = {
	request,
	uploadFile,
	loadImage,
	get: get,
	post: post,
}

/* 
使用方法 ：

1.在要使用的js文件导入
var HTTP = require('../../../common/utils/httpUtils.js');

2. 调用
HTTP.post('url', params).then(res => {
}).catch(error=>{
});

 */
